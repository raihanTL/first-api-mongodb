const express = require('express');
const res = require('express/lib/response');
const mongoConnect = require('../dbConnect/mongodbConnect').mongoConnect;
const getDb = require('../dbConnect/mongodbConnect').getDb;

module.exports = class users{
    // constructor(intern){
    //     this.name = intern.email;
    //     this.role = intern.role;
    //     this.email = intern.email;
    //     this.nid = intern.nid;
    // }

    static async addIntern(intern){
        const db = getDb();
        await db.collection('interns').insertOne(intern)
        .then(() => {
            console.log(`Intern ${intern.name} added successfully`);
        })
        .catch(err => {
            console.log(err);
        });
    }
    static async showInterns(){
        const db = getDb();
        let allInterns = await db.collection('interns').find().toArray();
        console.log(allInterns);
        return allInterns;
    }

    static async getInternByNid(internId){
        const db = getDb();
        console.log(typeof internId);
        try{
            let user = await db.collection('interns').find({nid: Number(internId)}).next();
            console.log(user);
            return user;
        }
        catch(err){
            console.log(err);
            return err;
        } 
    }

    static async deleteIntern(InternId){
        const db = getDb();
        try{
            await db.collection("interns").deleteOne({nid:InternId});
            console.log("deletion was successful");
        }
        catch(err){
            console.log(err);
            return err;
        }
    }

    static async updateDetail(updatedIntern){
        const db = getDb();
        try{ 
            let dbOp = await db.collection("interns").updateOne({nid:Number(updatedIntern.nid)}, {$set: updatedIntern});
            console.log("update was successful");
        }catch(err){
            console.log(err);
        }
    }
}