const express = require('express');

const router = express.Router();
const controller = require('../controllers/users');


router.get('/showAll/', controller.showInterns);

router.post('/remove', controller.removeIntern);

router.post('/update', controller.updateInternDetail);

router.get('/getDetail/:nid', controller.getIntern);

router.post('/', controller.createIntern);

module.exports = router;