const mongodb = require('mongodb');
const mongoClient = mongodb.MongoClient;

let _db;

const mongoConnect = (callback) => {
    mongoClient.connect('mongodb+srv://raihanTL:trucklagbe_8025@cluster0.8jqgm.mongodb.net/test?retryWrites=true&w=majority')
    .then(client => {
        console.log("Database connection was successful");
        _db = client.db();
        callback();
    })
    .catch(err => {
        console.log(err);
    })
};

const getDb = () => {
    if(_db){
        return _db;
    }

    console.log("No database found!!");
}

exports.mongoConnect = mongoConnect;
exports.getDb = getDb;