const express = require('express');
const bodyParser = require('body-parser');
const mongoConnect = require('./dbConnect/mongodbConnect').mongoConnect;

const usersRoutes = require('./routes/users.js');

const app = express();
app.use(bodyParser.json());

app.use('/users', usersRoutes);

app.get('/', (req, res) => {
    res.send("Hello from the server");
});

mongoConnect(()=>{
    app.listen(3000, () =>{
        console.log("Listening in port 3000...");
    });
})
